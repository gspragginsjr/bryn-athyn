<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bryn-athyn' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'l97vrq9nG(n*[^{dN,;cx?K&XUymAnjLq|l0e<}rl%uoSCg} 5j5m&5jNlrej%6I' );
define( 'SECURE_AUTH_KEY',  '$6B0?mrXUiz-YkWp|m5O#p%faV%kjO$fPJ*[f #C{Bl)18{gM0( n4-q`xo_Yp.t' );
define( 'LOGGED_IN_KEY',    '0W c^bSOJq)NY^Fx0c9}6Wh9ajvOQ#;,<Tk[MEi~()%Z.M?tW:R~m`SIsIS3+44[' );
define( 'NONCE_KEY',        'x= -{OmfHpowGUFl$*5Tmu-9DE!?<c;n4nvr4&R:95 BzzAHo~kNXq+<rLDEdYXI' );
define( 'AUTH_SALT',        ',N{!)_.s&+_WQjOt%~!I8gQO=Z-QgX)?,=-VKAQO+x%CW-T>[Ciesn}6CRw^!)hj' );
define( 'SECURE_AUTH_SALT', 'p90~sZP@z`M}+!lO^ U-%^EM.30-R4LtSm%N+sKX5)^|]nV/MK2Da@ahu,E,>!e*' );
define( 'LOGGED_IN_SALT',   'd&v..U[ ))|/Z3,=QtpQ9h+!+VHB|9Tqg>4=a|$`Xp9XoZAPTs;Kawv]t*N1I$L-' );
define( 'NONCE_SALT',       '/K97ai!?X:@:BiN6a;KS&vG~]Q_;h:RAEa4`F8E!!M;0F%^0S<SfM)qE5;@PT*bA' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
