module.exports = {
  title: "Icons",
  status: "wip",
  context: {
    name: 'example',
    size: 'md',
  },
  variants: [
    {
      name: "Small",
      context: {
        size: 'sm',
      }
    },
    {
      name: "Medium",
      context: {
        size: 'md',
      }
    },
    {
      name: "Large",
      context: {
        size: 'lg',
      }
    },
    {
      name: "X-Large",
      context: {
        size: 'xl',
      }
    }
  ]
};
