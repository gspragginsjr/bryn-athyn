module.exports = {
  title: "Color Palette",
  status: "wip",
  context: {
    groups: [
      {
        name: 'Hues',
        colors: [
          {
            variable: '$red',
            hex: '#c00',
          },
          {
            variable: '$blue',
            hex: '#1972e9',
          },
          {
            variable: '$darkblue',
            hex: '#1a60bd',
          },
          {
            variable: '$green',
            hex: '#7ed321',
          },
        ],
      },
      {
        name: 'Neutrals',
        colors: [
          {
            variable: '$neutral-lightest',
            hex: '#f4f4f4',
          },
          {
            variable: '$neutral-lighter',
            hex: '#dcdcdc',
          },
          {
            variable: '$neutral-light',
            hex: '#929292',
          },
          {
            variable: '$neutral-darker',
            hex: '#767676',
          },
          {
            variable: '$neutral-darkest',
            hex: '#2f2f2f',
          },
        ],
      },
    ],
  },
};
  