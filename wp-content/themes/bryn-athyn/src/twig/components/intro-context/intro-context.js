import 'waypoints/lib/noframework.waypoints';
import { TweenMax, Power4, TimelineMax } from 'gsap';

import './intro-context.scss';

const introContexts = document.querySelectorAll('.intro-context');
const introContextsWaypoints = [];

introContexts.forEach((introContext) => {
  introContextSetup(introContext);
  introContextsWaypoints.push(new Waypoint({ // eslint-disable-line
    element: introContext,
    handler() {
      introContextAnimation(introContext);
      this.destroy();
    },
    offset: '80%',
  }));
});

function introContextSetup(introContext) {
  const text = [].slice.call(introContext.querySelectorAll('h2, p'));
  const button = introContext.querySelector('.btn');

  // Setup all elements with 0 opacity
  TweenMax.set(text, {
    opacity: 0,
  });

  // Move the button 20 pixels down for the fly-in effect
  TweenMax.set(button, {
    y: 20,
  });
}

function introContextAnimation(introContext) {
  const introContextTimeline = new TimelineMax();
  const text = [].slice.call(introContext.querySelectorAll('h2, p'));

  // Let the animation fly
  introContextTimeline.add(TweenMax.to(text, 2, {
    opacity: 1,
    y: 0,
    ease: Power4.easeOut,
    stagger: 0.1,
  }));
}