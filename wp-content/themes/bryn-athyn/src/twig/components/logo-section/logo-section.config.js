module.exports = {
  title: 'Logo Section',
  status: 'wip',
  context: {
    heading: 'I Get to Work There?',
    paragraph: 'Career paths that start at Bryn Athyn go all over. Here are just a few companies and organizations where students have gone to work after graduation, with incredible stories to match.',
    logos: [
      {
        src: '/images/logo_mural-arts.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_deloitte.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_rutgers.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_ny-fashion.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_lilly-pulitzer.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_facebook.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_76ers.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_live-nation.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_jefferson.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_vanguard.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_comcast.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_bryn-athyn.svg',
        alt: 'test',
      },
      {
        src: '/images/logo_d.svg',
        alt: 'test',
      },
    ],
  },
};