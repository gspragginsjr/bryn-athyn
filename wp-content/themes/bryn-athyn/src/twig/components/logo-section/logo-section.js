import 'waypoints/lib/noframework.waypoints';
import { TweenMax, Power4, TimelineMax } from 'gsap';

import './logo-section.scss';

const logoSections = document.querySelectorAll('.logo-section');
const logoSectionWaypoints = [];

logoSections.forEach((logoSection) => {
  logoSectionSetup(logoSection);
  logoSectionWaypoints.push(new Waypoint({ // eslint-disable-line
    element: logoSection,
    handler() {
      logoSectionAnimation(logoSection);
      this.destroy();
    },
    offset: '80%',
  }));
});

function logoSectionSetup(logoSection) {
  const text = [].slice.call(logoSection.querySelectorAll('h2, p, img'));

  // Setup all elements with 0 opacity
  TweenMax.set(text, {
    opacity: 0,
  });
}

function logoSectionAnimation(logoSection) {
  const logoSectionTimeline = new TimelineMax();
  const text = [].slice.call(logoSection.querySelectorAll('h2, p, img'));

  // Let the animation fly
  logoSectionTimeline.add(TweenMax.to(text, 2, {
    opacity: 1,
    y: 0,
    ease: Power4.easeOut,
    stagger: 0.1,
  }));
}