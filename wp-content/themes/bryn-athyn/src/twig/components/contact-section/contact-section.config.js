module.exports = {
  title: 'Contact Section',
  status: 'wip',
  context: {
    heading: "Let's <strong>Listen Before We</strong> Talk.",
    image: {
      src: 'https://via.placeholder.com/700x450',
      alt: 'test image',
    },
    content: "<p>If you've made it this far, we're guessing you like what you see. You've probably had some questions answered and thought of about a million more. You can hear more straight from the lions' mouths here (link). While you could continue down the internet rabbit hole, we encourage you to schedule a call with an admissions counselor (link). But don't be surprised if you have a new friend by the end of your conversation. Between the size of our student population and the size of the average heart at Bryn Athyn, our admissions counselors actually take the time to get to know you and your goals on a personal level. That can make all the difference throughout your admissions process.</p>",
  },
};