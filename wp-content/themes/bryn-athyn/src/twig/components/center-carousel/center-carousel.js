import $ from 'jquery';
import 'slick-carousel';

import './center-carousel.scss';

const centerCarousel = document.querySelectorAll('[data-center-carousel');

centerCarousel.forEach((slider) => {
  const centerCarouselWindow = slider.querySelector('[data-center-carousel-window]');
  const controls = slider.querySelector('[data-center-carousel-controls]');
  const contentWrap = slider.querySelector('[data-center-carousel-content-wrap]');
  const contentBlocks = slider.querySelectorAll('[data-center-carousel-content]');
  const dotsWrap = slider.querySelector('[data-center-carousel-dots-wrap]');
  const chevron = slider.querySelector('.center-carousel__base-chevron');
  let arrows;

  const initArrows = function() {
    arrows = slider.querySelectorAll('.slick-arrow');

    contentBlocks[0].classList.add('active');

    arrows.forEach((a) => {
      const c = chevron.cloneNode(true);
      a.prepend(c);
    });
  }

  $(centerCarouselWindow).on('init', () => {
    initArrows();

    chevron.remove();
    resizeControls(slider, controls);
    resizeControls(slider, contentWrap);
  }).on('setPosition', () => {
    resizeControls(slider, controls);
    resizeControls(slider, contentWrap);
  }).on('beforeChange', (event, s, currentSlide, nextSlide) => {
    contentBlocks.forEach((b) => {
      b.classList.remove('active');
    });

    contentBlocks[nextSlide].classList.add('active');
  }).on('breakpoint', () => {
    initArrows();
  });

  $(centerCarouselWindow).slick({
    centerMode: true,
    slidesToShow: 3,
    centerPadding: '0',
    rows: 0,
    appendArrows: controls,
    appendDots: dotsWrap,
    dots: true,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
});

function resizeControls(slider, target) {
  const centerSlide = slider.querySelector('.slick-center');
  const centerSlideWidth = centerSlide.offsetWidth;

  target.style.width = centerSlideWidth + 'px';
}
