const image = {
  alt: 'Sample Alt Text',
  src: 'https://via.placeholder.com/900x700',
};

const image2 = {
  alt: 'Sample Alt Text',
  src: 'https://via.placeholder.com/900x700',
};

module.exports = {
  title: 'Center Carousel',
  status: 'wip',
  context: {
    heading: 'Our Favorite Places',
    slides: [
      {
        heading: 'A Local Spot',
        paragraph: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
        image,
      },
      {
        heading: 'A Historic Building',
        paragraph: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent scelerisque risus non mauris semper, a tincidunt sem lobortis. Ut sagittis metus tortor, vel luctus neque tincidunt nec. Ut et purus eget odio consectetur scelerisque nec sed felis. Phasellus ultrices convallis varius. Nulla eleifend massa magna, at tincidunt lectus consequat in.',
        image: image2,
      },
      {
        heading: 'A Student Favorite',
        paragraph: 'Praesent scelerisque risus non mauris semper, a tincidunt sem lobortis. Ut sagittis metus tortor, vel luctus neque tincidunt nec. Ut et purus eget odio consectetur scelerisque nec sed felis. Phasellus ultrices convallis varius. Nulla eleifend massa magna, at tincidunt lectus consequat in. Morbi nec malesuada lacus. In fermentum, nunc ac interdum vestibulum, felis dolor dapibus mauris, sed consectetur ligula ex eget metus.',
        image,
      },
      {
        heading: 'World Famous Venue',
        paragraph: 'Ut et purus eget odio consectetur scelerisque nec sed felis. Phasellus ultrices convallis varius. Nulla eleifend massa magna, at tincidunt lectus consequat in. Morbi nec malesuada lacus. In fermentum, nunc ac interdum vestibulum, felis dolor dapibus mauris, sed consectetur ligula ex eget metus.',
        image: image2,
      },
    ],
  },
};
