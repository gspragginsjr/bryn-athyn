import 'waypoints/lib/noframework.waypoints';
import { TweenMax, Power4, TimelineMax } from 'gsap';

import './hero.scss';

const heroes = document.querySelectorAll('.hero');
const heroWaypoints = [];

heroes.forEach((hero) => {
  heroSetup(hero);
  heroWaypoints.push(new Waypoint({ // eslint-disable-line
    element: hero,
    handler() {
      heroAnimation(hero);
      this.destroy();
    },
    offset: '80%',
  }));
});

function heroSetup(hero) {
  const text = [].slice.call(hero.querySelectorAll('h1'));

  // Setup all elements with 0 opacity
  TweenMax.set(text, {
    opacity: 0,
  });
}

function heroAnimation(hero) {
  const heroTimeline = new TimelineMax();
  const text = [].slice.call(hero.querySelectorAll('h1'));

  // Let the animation fly
  heroTimeline.add(TweenMax.to(text, 2, {
    opacity: 1,
    y: 0,
    ease: Power4.easeOut,
    stagger: 0.1,
  }));
}