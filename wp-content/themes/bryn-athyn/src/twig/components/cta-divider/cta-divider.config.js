module.exports = {
  title: 'CTA Divider',
  status: 'wip',
  context: {
    heading: 'The Benefits of Bryn Athyn to You.',
    paragraph: 'Need some copy here to fill this space and encourage ppl to explore he jump links.',
    ctas: [
      {
        text: 'Our Size',
        href: '#our-size',
      },
      {
        text: 'Our People',
        href: '#our-people',
      },
      {
        text: 'Our Purpose',
        href: '#our-purpose',
      },
      {
        text: 'Our Value',
        href: '#our-value',
      },
    ],
  },
};