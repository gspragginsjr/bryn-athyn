const image = {
  alt: 'Sample Alt Text',
  src: 'https://via.placeholder.com/770x490',
};

const image2 = {
  alt: 'Sample Alt Text',
  src: 'https://via.placeholder.com/780x500',
};

module.exports = {
  title: 'Headline Carousel',
  status: 'wip',
  context: {
    heading: 'The Future Generator',
    slides: [
      {
        heading: 'Become an deer-studying explorer',
        image,
      },
      {
        heading: 'This is another nifty headline for Bryn Athyn.',
        image: image2,
      },
      {
        heading: 'Third slide here. This is a test.',
        image,
      },
      {
        heading: 'GSAP animations are fun.',
        image: image2,
      },
    ],
  },
};