import $ from 'jquery';
import 'slick-carousel';

import './headline-carousel.scss';

const headlineCarousel = document.querySelectorAll('[data-headline-carousel');

headlineCarousel.forEach((slider) => {
  const headlineCarouselWindow = slider.querySelector('[data-headline-carousel-window]');
  const headlineCarouselHeadings = slider.querySelectorAll('[data-headline-carousel-heading]');

  $(headlineCarouselWindow).on('init', ()=> {
    headlineCarouselHeadings[0].classList.add('active');
  }).on('beforeChange', (event, s, currentSlide, nextSlide) => {
    headlineCarouselHeadings.forEach((b) => {
      b.classList.remove('active');
    });

    headlineCarouselHeadings[nextSlide].classList.add('active');
  });

  $(headlineCarouselWindow).slick({
    infinite: true,
    slidesToScroll: 1,
    rows: 0,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
});