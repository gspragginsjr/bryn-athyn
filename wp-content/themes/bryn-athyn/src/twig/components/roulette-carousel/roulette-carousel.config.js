const image = {
  src: 'https://via.placeholder.com/960x1050',
  alt: 'test image',
};

module.exports = {
  title: 'Roulette Carousel',
  status: 'wip',
  context: {
    column_1: [
      {
        image,
        heading: 'A Deer-Studying',
      },
      {
        image,
        heading: 'An Adventurous',
      },
      {
        image,
        heading: 'A Political',
      },
      {
        image,
        heading: 'A Published',
      },
    ],
    column_2: [
      {
        image,
        heading: 'Explorer',
      },
      {
        image,
        heading: 'Entrepreneur',
      },
      {
        image,
        heading: 'Physicist',
      },
      {
        image,
        heading: 'Gardener',
      },
    ],
  },
  variants: [
    {
      name: 'Plus',
      status: 'wip',
      context: {
        plus: true,
      },
    }
  ]
};