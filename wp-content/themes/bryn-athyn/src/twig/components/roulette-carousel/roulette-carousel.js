import $ from 'jquery';
import 'slick-carousel';

import './roulette-carousel.scss';

const rouletteCarousel = document.querySelectorAll('[data-roulette-carousel');

rouletteCarousel.forEach((slider) => {
  const rouletteCarouselWindows = slider.querySelectorAll('[data-roulette-carousel-window]');
  const shuffleBtn = slider.querySelector('[data-roulette-carousel-btn]');
  const carouselObjects = [];
  let mousedOver = false;
  let slideCount;

  slider.addEventListener('mouseenter', () => {
    mousedOver = true;
  });

  slider.addEventListener('mouseleave', () => {
    mousedOver = false;
  });

  rouletteCarouselWindows.forEach((window) => {
    slideCount = window.querySelectorAll('.roulette-carousel__slide').length;

    carouselObjects.push(
      $(window).slick({
        rows: 0,
        arrows: false,
        draggable: false,
        infinite: true,
        vertical: true,
      }));
  });

  shuffleBtn.addEventListener('click', (e) => {
    carouselObjects.forEach((o) => {
      const currentSlide = o.slick('getSlick').currentSlide;
      let newSlide = Math.floor(Math.random() * (slideCount + 1));

      while (currentSlide == newSlide) {
        newSlide = Math.random() * (slideCount + 1);
      }

      o.slick('slickGoTo', newSlide);
    });
  });

  setInterval(() => {
    if (!mousedOver) {
      shuffleBtn.click();
    }
  }, 3000);
});