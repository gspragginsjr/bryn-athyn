const image = {
  alt: 'Sample Alt Text',
  src: 'https://via.placeholder.com/530x430',
};

module.exports = {
  title: 'Article Section',
  status: 'wip',
  context: {
    items: [
      {
        image,
        paragraph: "I don't want to be a ghost writer for my life. I want to write my own story.",
        name: "Chantal Farmer, '20",
      },
      {
        image,
        paragraph: "I don't want to be a ghost writer for my life. I want to write my own story.",
        name: "Chantal Farmer, '20",
      },
      {
        image,
        paragraph: "I don't want to be a ghost writer for my life. I want to write my own story.",
        name: "Chantal Farmer, '20",
      },
    ],
  },
};