import 'waypoints/lib/noframework.waypoints';
import { TweenMax, Power4, TimelineMax } from 'gsap';

import './image-context.scss';

const imageContexts = document.querySelectorAll('.image-context');
const imageContextWaypoints = [];

imageContexts.forEach((imageContext) => {
  imageContextSetup(imageContext);

  imageContextWaypoints.push(new window.Waypoint({
    element: imageContext,
    handler() {
      imageContextAnimation(imageContext);
      this.destroy();
    },
    offset: '80%',
  }));
});

function imageContextSetup(imageContext) {
  const shade = imageContext.querySelector('.image-context__shade');
  const text = imageContext.querySelectorAll('.image-context__decor, .image-context__big-window, .image-context__large-heading, .image-context__decor-window, .image-context__heading, .image-context__subheading, p, li');

  // Setup video shade
  TweenMax.set(shade, {
    width: '100%',
  });

  // Setup all text elements with 0 opacity
  TweenMax.set(text, {
    opacity: 0,
    // y: 20,
  });
}
function imageContextAnimation(imageContext) {
  const imageContextTimeline = new TimelineMax();
  const shade = imageContext.querySelector('.image-context__shade');
  const text = imageContext.querySelectorAll('.image-context__decor, .image-context__big-window, .image-context__large-heading, .image-context__decor-window, .image-context__heading, .image-context__subheading, p, li');

  imageContextTimeline.add(TweenMax.to(shade, 0.8, {
    width: '0%',
    ease: Power4.easeOut,
  }));

  TweenMax.to(text, 1.4, {
    opacity: 1,
    // y: 0,
    ease: Power4.easeOut,
    stagger: 0.1,
  });
}