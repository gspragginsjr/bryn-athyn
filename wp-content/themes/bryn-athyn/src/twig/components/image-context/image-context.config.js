const image = {
  alt: 'Sample Alt Text',
  src: 'https://via.placeholder.com/600x600',
};

module.exports = {
  title: 'Image Context',
  status: 'wip',
  context: {
    subheading: 'Our Purpose',
    heading: "Declare Your<br>Life's Major Purpose.",
    text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
    image,
  },
  variants: [
    {
      name: 'Reversed',
      context: {
        reversed: true,
      },
    },
    {
      name: 'Plus Version',
      context: {
        heading: 'By Going Beneath<br>the Surface',
        subheading: 'At First Glance, We May Look Like Any College of Your Dreams.',
        text: '<p>Beautiful campus, rigorous classes, plentiful extracurriculars. But when you get beyond all that, you’ll find Bryn Athyn isn’t like other schools. What makes us different is something you can’t see, but you’ll feel it: a sense of camaraderie and belonging links together every person on this campus. Connections are deeper here—that’s what Bryn Athyn is really all about.</p><p>Keep scrolling to dive deeper into how we do things at Bryn Athyn. For even more depth, check out (link) or imagine your life here with a real human on the other end—one of our admissions counselors, who you can talk to here.</p>',
        ctas: [
          {
            text: 'Talk to one of our admission counselors',
            href: '/',
          },
          {
            text: 'Already know this is the place for you?',
            href: '/',
          },
        ],
        reversed: true,
        plus: true,
      },
    },
  ],
};