import 'waypoints/lib/noframework.waypoints';
import { TweenMax, Power4, TimelineMax } from 'gsap';

import './image-context-section.scss';

const imageContextSections = document.querySelectorAll('.image-context-section');
const imageContextSectionWaypoints = [];

imageContextSections.forEach((imageContextSection) => {
  imageContextSectionSetup(imageContextSection);
  imageContextSectionWaypoints.push(new Waypoint({ // eslint-disable-line
    element: imageContextSection,
    handler() {
      imageContextSectionAnimation(imageContextSection);
      this.destroy();
    },
    offset: '80%',
  }));
});

function imageContextSectionSetup(imageContextSection) {
  const text = [].slice.call(imageContextSection.querySelectorAll('.image-context-section__heading, .image-context-section__p'));

  // Setup all elements with 0 opacity
  TweenMax.set(text, {
    opacity: 0,
  });
}

function imageContextSectionAnimation(imageContextSection) {
  const imageContextSectionTimeline = new TimelineMax();
  const text = [].slice.call(imageContextSection.querySelectorAll('.image-context-section__heading, .image-context-section__p'));

  // Let the animation fly
  imageContextSectionTimeline.add(TweenMax.to(text, 2, {
    opacity: 1,
    y: 0,
    ease: Power4.easeOut,
    stagger: 0.1,
  }));
}