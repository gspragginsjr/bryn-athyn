const image = {
  alt: 'Sample Alt Text',
  src: 'https://via.placeholder.com/600x600',
};

module.exports = {
  title: 'Image Context Section',
  status: 'wip',
  context: {
    items: [
      {
        subheading: 'Our Size',
        heading: "We See Your Student-to-Faculty Ratio and Raise You A Student-to-Acre Ratio",
        text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
        image,
      },
      {
        subheading: 'Our People',
        heading: "Every Professor Gives Grades.<br>Great Professors Give You Meaning.",
        text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
        image,
      },
      {
        subheading: 'Our Purpose',
        heading: "Declare Your<br>Life's Major Purpose.",
        text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
        image,
      },
      {
        subheading: 'Our Purpose',
        heading: "Declare Your<br>Life's Major Purpose.",
        text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
        image,
      },
    ],
  },
  variants: [
    {
      name: 'Dark',
      context: {
        dark: true,
        heading: 'Peace of Mind, Body, and Spirit',
        paragraph: "We might talk a lot about spirit here, but we're still in touch with what you need out in the real world. You don't have to wait until you graduate to make an impact; experiental internships are part of everyone's core curriculum. Should grad school be your path, Bryn Athyn offers ample hands-on research opportunities. Our New Church affiliation enables students to study abroad at sister institutions in France and Germany, and faculty led trips could have you studying sea life in the Cayman Islands or getting up up close and personal with ancient Greek History. And no matter where you go, that same sense of Bryn Athyn belonging will travel with you.",
        items: [
          {
            subheading: 'Our Size',
            heading: "We See Your Student-to-Faculty Ratio and Raise You A Student-to-Acre Ratio",
            text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
            image,
          },
          {
            subheading: 'Our People',
            heading: "Every Professor Gives Grades.<br>Great Professors Give You Meaning.",
            text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
            image,
          },
          {
            subheading: 'Our Purpose',
            heading: "Declare Your<br>Life's Major Purpose.",
            text: "<p>College should give you the space to figure out what's important. Whether you're an interdisciplinary or business major, Bryn Athyn gives you the skills to make a positive impact in the world. It's not about just looking good on a resume (though employers undoubtedly reap the benefits of hiring our graduates). We welcome all faiths into our New Church tradition, with a focus on spiritual growth for a rewarding life.</p>",
            image,
          },
        ],
      },
    }
  ],
};