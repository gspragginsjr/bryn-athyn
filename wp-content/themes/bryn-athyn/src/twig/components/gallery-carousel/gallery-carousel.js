import $ from 'jquery';
import 'slick-carousel';

import './gallery-carousel.scss';

const galleryCarousel = document.querySelectorAll('[data-gallery-carousel');

galleryCarousel.forEach((slider) => {
  const galleryCarouselWindow = slider.querySelector('[data-gallery-carousel-window]');
  const dotsWrap = slider.querySelector('[data-gallery-carousel-dots-wrap]');
  const chevron = slider.querySelector('.gallery-carousel__base-chevron');
  let arrows;

  $(galleryCarouselWindow).on('init', () => {
    arrows = slider.querySelectorAll('.slick-arrow');

    arrows.forEach((a) => {
      const c = chevron.cloneNode(true);
      a.prepend(c);
    });

    chevron.remove();
  });

  $(galleryCarouselWindow).slick({
    rows: 0,
    dots: true,
    appendDots: dotsWrap,
    autoplay: true,
    autoplaySpeed: 5000,
  });
});