const image = {
  src: 'https://via.placeholder.com/950x750',
  alt: 'test image',
};

module.exports = {
  title: 'Gallery Carousel',
  status: 'wip',
  context: {
    heading: 'Life at Bryn Athyn',
    slides: [
      {
        image,
        heading: 'We fill our schedule with things that fulfill us.',
        paragraph: 'For some, that means taking on a leadership role in just about every club, student organization, and team there is. Will you sling grilled cheese to end hunger with FeelGood, or jam with your professors in Bryn Athyn College, ROCKS!, our aptly named student-faculty rock group? For others, the ideal schedule is one thats extra focused on school and personal development. This is your journey, and you should go about it at your pace.',
      },
      {
        image,
        heading: 'Athletics are a huge part of our little school.',
        paragraph: "All that character we're building? It makes for some incredible sportpersonship. Like most parts of life at Bryn Athyn, this is not a place to sit the bench. Half of all students participate in athletics, competeing in the sports they love at the DIII level. Which means that the other 50 percent of student population has a very important position; make, some noose. GO LIONS!",
      },
    ],
  },
}