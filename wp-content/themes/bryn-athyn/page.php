<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/templates/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();

$timber_post     = new Timber\Post();
$context['post'] = $timber_post;
$context['global_email'] = get_field('email', 'options');
$context['global_phone'] = get_field('phone', 'options');
$context['global_header_ctas'] = get_field('header_ctas', 'options');
$context['global_contact_ctas'] = get_field('contact_ctas', 'options');
$context['global_contact_outro'] = get_field('outro', 'options');
$context['global_contact_image'] = get_field('contact_image', 'options');

Timber::render( array( 'page-' . $timber_post->post_name . '.twig', 'page.twig' ), $context );
